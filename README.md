Tiny simple TOTP token generator/validator
=======

This is a simple (re)implementation of a TOTP Token generator & validator written in C
and roughly following the references:

* [RFC6238](https://tools.ietf.org/html/rfc6238)
* [RFC4226](https://tools.ietf.org/html/rfc4226)

What does this tool do?
* generates TOTP codes between 4 and 10 digits (default 6)
* validates TOTP codes and launch a shell if there is a match (for use in `~/.ssh/authorized_keys`)
* use base32 secret key (autopadding with "=")
* supports SHA-1, SHA-256 and SHA-512 hash functions (default SHA-1)
* generation and validation can use up to 10 TOTP codes before and after the current code (default 0)
* formatting options for easy reading of the TOTP code (digit grouping)

Tested with physical tokens (Token2 [C302-i](https://www.token2.eu/shop/product/token2-c302-i-programmable-hardware-token-iphone-compatible), [OTPC-P2-i-NB](https://www.token2.eu/shop/product/token2-otpc-p2-i-programmable-card-with-restricted-time-sync-nonbranded) and [C203](https://www.token2.eu/shop/product/token2-c203-sha256-totp-hardware-token)), with [Aegis Authenticator](https://github.com/beemdevelopment/Aegis) Android app and with `oathtool`.

Note : Do not blindly use this tool, it is not perfect, read the code.

Credits : This work is initially based on [Francesco Pantano's c_otp code](https://github.com/fmount/c_otp).
