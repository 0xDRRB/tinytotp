#ifndef TOTPSHA_H
#define TOTPSHA_H

#define TOTP_SHA1      20
#define TOTP_SHA256    32
#define TOTP_SHA512    64

#endif
