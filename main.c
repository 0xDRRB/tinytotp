#include <time.h>
#include <math.h>
#include <ctype.h>
#include <unistd.h>
#include "rfc4226.h"
#include "rfc6238.h"
#include "utils.h"
#include "hmac.h"

#define DIGITS           6
#define DIGITS_MIN       4
#define DIGITS_MAX      10
#define BEFOREAFTERMIN   1
#define BEFOREAFTERMAX  10
#define VALIDITY        30
#define VERSION        0.2

uint32_t
totp(uint8_t *k, size_t keylen, int digits, int tthmac, int t0)
{
    time_t t = floor((time(NULL) + t0) / VALIDITY);

    return TOTP(k, keylen, t, digits, tthmac);
}

void
usage(char *binname)
{
	printf("TinyTOTP %.1f\n", VERSION);
	printf("Copyright (c) 2022 - Denis Bodor\n\n");
	printf("Usage : %s [OPTIONS]\n", binname);
	printf(" -b secretkey   use this base32 encoded secret key\n");
	printf(" -d digits_num  set number of OTP digits (between 4 and 10, default 6)\n");
	printf(" -2 | -3        show OTP in 2-digit or 3-digit grouping (default no grouping)\n");
	printf(" -m             use SHA-256 HMAC (default SHA-1)\n");
	printf(" -l             use SHA-512 HMAC (default SHA-1)\n");
	printf(" -B number      number of additional before OTPs to generate or validate against\n");
	printf(" -A number      number of additional after OTPs to generate or validate against\n");
	printf(" -q             be quiet. Just display the OTP(s) and nothing else\n");
	printf(" -a             ask for validation OTP and execl() a shell (~/.ssh/authorized_keys)\n");
	printf(" -v             show version\n");
}

void
proceed()
{
	if (getenv("SSH_ORIGINAL_COMMAND") != NULL) {
		execl("/bin/sh", "/bin/sh", "-c", getenv("SSH_ORIGINAL_COMMAND"), NULL);
	} else {
		execl(getenv("SHELL"), "-", NULL);
	}
}

void
dispwithgap(uint32_t code, int digits, int dispgap)
{
	char dispcode[32];

	if (!dispgap) {
		printf("%0*u", digits, code);
		return;
	}

	snprintf(dispcode, digits+1, "%0*u", digits, code);
	for (int i=0; i<strlen(dispcode); i++) {
		printf("%c", dispcode[i]);
		if (((i + 1) % dispgap) == 0)
			printf(" ");
	}
}

int
main(int argc, char *argv[])
{
    size_t len;
    size_t keylen;
    uint8_t *k;
    char *key = NULL;
    int opt;
	uint32_t codes[21];
	char *endptr;
	char input[10];
	uint32_t inputn;
	char *padkey;
	size_t padkeylen;
	int optask = 0;
	int digits = DIGITS;
	int dispgap = 0;
	int optquiet = 0;
	int opthmac = TOTP_SHA1;
	int optbefore = 0;
	int optafter = 0;

    if (argc <= 1) {
        fprintf(stderr, "Provide at least one argument\n");
        return EXIT_FAILURE;
    }

    while ((opt = getopt(argc, argv, "b:d:B:A:a23mlqhv")) != -1) {
        switch(opt) {
        case 'b':
            key = strdup(optarg);
            break;
		case 'd':
			digits = (int)strtol(optarg, &endptr, 10);
			if (endptr == optarg || digits < DIGITS_MIN || digits > DIGITS_MAX) {
				fprintf(stderr, "You must specify a valid number (%d to %d)\n", DIGITS_MIN, DIGITS_MAX);
				return EXIT_FAILURE;
			}
			break;
		case 'B':
			optbefore = (int)strtol(optarg, &endptr, 10);
			if (endptr == optarg || optbefore < BEFOREAFTERMIN || optbefore > BEFOREAFTERMAX) {
				fprintf(stderr, "You must specify a valid number (%d to %d)\n", BEFOREAFTERMIN, BEFOREAFTERMAX);
				return EXIT_FAILURE;
			}
			break;
		case 'A':
			optafter = (int)strtol(optarg, &endptr, 10);
			if (endptr == optarg || optafter < BEFOREAFTERMIN || optafter > BEFOREAFTERMAX) {
				fprintf(stderr, "You must specify a valid number (%d to %d)\n", BEFOREAFTERMIN, BEFOREAFTERMAX);
				return EXIT_FAILURE;
			}
			break;
		case 'a':
			optask = 1;
			break;
		case '2':
			dispgap = 2;
			break;
		case '3':
			dispgap = 3;
			break;
		case 'm':
			opthmac = TOTP_SHA256;
			break;
		case 'l':
			opthmac = TOTP_SHA512;
			break;
		case 'q':
			optquiet = 1;
			break;
		case 'h':
			usage(argv[0]);
			return EXIT_SUCCESS;
        case 'v':
            printf("TinyTOTP %.1f\n", VERSION);
            return 0;
        default:
            usage(argv[0]);
            return EXIT_FAILURE;
        }
    }

	if (!key) {
		fprintf(stderr, "You must provide a b32_secretkey!\n");
		return EXIT_FAILURE;
	}

	len = strlen(key);

	if (!len) {
		fprintf(stderr, "Invalid base32 data!\n");
		return(EXIT_FAILURE);
	}

	// key padding
	padkeylen = len;
	if (len % 8)
		padkeylen += 8-(len % 8);

	if (padkeylen != len) {
		if ((padkey = (char *) malloc((padkeylen+1)*sizeof(char))) == NULL) {
			fprintf(stderr, "malloc error!\n");
			return EXIT_FAILURE;
		}
		memset(padkey, 0, (padkeylen+1)*sizeof(char));
		// fill with '='
		for (int i=0; i < padkeylen; i++) {
			padkey[i] = '=';
		}
		// copy undersized key
		for (int i=0; i < len; i++) {
			padkey[i] = key[i];
		}
		// replace key
		free(key);
		key = padkey;
		len = padkeylen;
	}

	if (validate_b32key((unsigned char *)key, len) == 1) {
		fprintf(stderr, "%s: invalid base32 secret\n", key);
		return EXIT_FAILURE;
	}

	k = (uint8_t *)key;
	keylen = decode_b32key(&k, len);

#ifdef DEBUG
	printf("hexkey (%zu): ", keylen);
	for (int i = 0; i < keylen; i++) {
		printf("%02x ", k[i]);
	}
	printf("\n");
#endif

	for (int i=10-optbefore; i<optafter+11; i++) {
		codes[i] = totp(k, keylen, digits, opthmac, (i-10)*30);
	}

	if (optask) {
		fprintf(stderr, "Enter the validation code: ");
		if (fgets(input, sizeof(input), stdin) == NULL) {
			exit(EXIT_FAILURE);
		}

		inputn = (uint32_t)strtol(input, &endptr, 10);
		if (endptr == input || inputn > (pow(10, digits)-1)) {
			fprintf(stderr, "You must specify a valid number\n");
			if (key) free(key);
			return EXIT_FAILURE;
		}

		for (int i=10-optbefore; i<optafter+11; i++) {
			if (inputn == codes[i]) {
				proceed();
				free(key);
				return EXIT_SUCCESS;
			}
		}
		fprintf(stderr, "Invalid code!\n");

		free(key);
		return EXIT_FAILURE;
	}

	if (!optquiet && !optbefore && !optafter)
		printf("The OTP code is: ");

	for (int i=10-optbefore; i<optafter+11; i++) {
		if ((optbefore || optafter) && !optquiet && i == 10)
			printf("\e[1m");
		dispwithgap(codes[i], digits, dispgap);
		if ((optbefore || optafter) && !optquiet && i == 10)
			printf(" <\e[0m");
		printf("\n");
	}


	free(key);

	return EXIT_SUCCESS;
}
